# BMF font skeleton

A base template from which to create new BMF fonts. Read the [Type:Bits
documentation](https://typebits.gitlab.io/new-font/) for the details on how to
make your own font from this starter scaffold.

The below links will only work when you've added glyphs and pushed your fork to
GitLab. Be sure to change all instances of `skeleton` and `Skeleton` to your
font's name in the links.

![Font preview](https://gitlab.com/typebits/font-skeleton/-/jobs/artifacts/master/raw/Skeleton-Regular.png?job=build-font)
  
[See it in action](https://typebits.gitlab.io/font-skeleton/)

Download formats:

* [TrueType (.ttf)](https://gitlab.com/typebits/font-skeleton/-/jobs/artifacts/master/raw/Skeleton-Regular.ttf?job=build-font)
* [OpenType (.otf)](https://gitlab.com/typebits/font-skeleton/-/jobs/artifacts/master/raw/Skeleton-Regular.otf?job=build-font)
* [FontForge (.sfd)](https://gitlab.com/typebits/font-skeleton/-/jobs/artifacts/master/raw/Skeleton-Regular.sfd?job=build-font)

## Authors

Include a list of each of the font's authors. For consistency, we recommend using a bullet list and add a link to each author's website if desired, e.g:

* [Author 1](https://typebits.gitlab.io)
* [Author 2](https://typebits.gitlab.io)
* [Author 3](https://typebits.gitlab.io)

## License

This font is made available under the [Open Font License](https://scripts.sil.org/OFL).
